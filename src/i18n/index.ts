import { createI18n } from "vue-i18n"
import { english } from "./english"
import { french } from "./french"

const messages = {
  en_us: english,
  fr_fr: french
}

export default createI18n({
  locale: 'en_us',
  messages: messages,
})