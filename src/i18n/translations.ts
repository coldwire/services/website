export type Translation = {
  VIEWS: {
    HOME: {
      TITLE: String,
      KNOWLEDGE_CARD: {
        TITLE: String,
        DESCRIPTION: String,
        BUTTON: String
      },
      SERVICES_CARD: {
        TITLE: String,
        DESCRIPTION: String,
      }
    },
    DONATE: {
      TITLE: String

      CRYPTO: String,
      CARD: String,
      BANK: String,
      BANK_INFO: String,

      TEXT_1: String,
      TEXT_2: String,
      TEXT_3: String,
      TEXT_4: String,
      TEXT_5: String
    },
    ABOUT: {
      TITLE: String
    },
    CONTRIBUTE: {
      TITLE: String
    }
  },

  LINKS: {
    RESSOURCES: String,
    CONTRIBUTE: String,
    ABOUT: String,
    DONATE: String,
  }

  MENUS: {
    RESSOURCES: {
      KNOWLEDGE_TITLE: String,
      SERVICES_TITLE: String,
      SOON: String
    }
  }

  RESSOURCES: {
    ZINE: {
      TITLE: String,
      DESCRIPTION: String
    },
    BLOC: String
    CINNY: String
  }

  FOOTER: {
    FOLLOW_US: String,
    TERMS: String,
    LICENSE: String
  }
}