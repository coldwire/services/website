import type { Translation } from "./translations"

export const english: Translation = {
  VIEWS: {
    HOME: {
      TITLE: 'Resilient, self-managed, anonymous network fighting repression and censorship',
      KNOWLEDGE_CARD: {
        TITLE: 'Knowledge',
        DESCRIPTION: 'Coldwire aim to provide and centralize basic and important knowlege about online and on the ground self-defense, for activist, journalists and at risk minorities.',
        BUTTON: 'Zine: Collective Defense'
      },
      SERVICES_CARD: {
        TITLE: 'Services',
        DESCRIPTION: 'Alongside our work on spreading knowledge, we are developping and providing decentralized, encrypted and anonymous services for everyone.',
      }
    },
    DONATE: {
      TITLE: 'Help us maintaining the infrastructure and fund others autonomous tech collectives!',

      CRYPTO: 'Cryptocurrencies',
      CARD: 'Credit card',
      BANK: 'Bank transfer',
      BANK_INFO: '(Important for our fiscal host!)',

      TEXT_1: 'Coldwire is quiet expensive to maintain and still, we need money to archive our full autonomy.',
      TEXT_2: 'Actually we have a dedicated server, it cost us 64€/months, we have 3 domain names (coldwire.org, coldnet.org, cdw.re) that costs us 15€/years each and then we have our proxy server in the Netherland we pay 5€/months and another one for the same price for managing the network. In total we pay 77.75€ per months',
      TEXT_3: 'The problem is that we don\'t own any of these servers, which can be dangerous in the long term, even if everything sensitive is encrypted, IPs are logged by the hosting providers, etc. Our goal is to reach our full autonomy by having our own server room with our own hardware and why not our own AS (Autonomou Number) with our own IPs. But all theses things cost a lost, renting a place, buying refurbished servers, switchs and brand new disks, paying for unlimited internet, electricity, etc is super expensive. Of course all the tools we are developing are self-hostable and we encourage you to do host your own instances! but some groups or people can\'t afford these costs or don\'t have the skills to setup a server, this is why we need a reliable infrastructure we can trust',
      TEXT_4: 'Outside of that we would really love to be able to donate or provide services (virtual machines, web servers) to others collectives, projects and associations that have the same values as us',
      TEXT_5: 'By donating your are allowing us to actively continue our mission of developing new resilient, safe, free and accessible softwares, to help people and collectives all around the world to organize safely.'
    },
    ABOUT: {
      TITLE: 'Who are we, what do we do and why it is a legitimate question?'
    },
    CONTRIBUTE: {
      TITLE: 'Interested in security, anticapitalist, not transphobic, writing code? Join us and contribute!'
    }
  },

  LINKS: {
    RESSOURCES: 'Ressources',
    CONTRIBUTE: 'Contribute',
    ABOUT: 'About',
    DONATE: 'Donate',
  },

  MENUS: {
    RESSOURCES: {
      KNOWLEDGE_TITLE: 'Knowledge',
      SERVICES_TITLE: 'Services',
      SOON: 'More coming soon!'
    }
  },

  RESSOURCES: {
    ZINE: {
      TITLE: 'Collective Defense',
      DESCRIPTION: 'Zine about defensive tactics online and on the ground.'
    },
    BLOC: 'Encrypted, decentralized and anonymous file storage',
    CINNY: 'Secure and independent communication, connected via Matrix'
  },

  FOOTER: {
    FOLLOW_US: 'Follow us on Mastodon',
    TERMS: 'Terms of service',
    LICENSE: 'All material is licensed under CC BY-NC-SA 2.0 (2022)'
  }
}