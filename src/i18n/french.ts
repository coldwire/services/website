import type { Translation } from "./translations"

export const french: Translation = {
  VIEWS: {
    HOME: {
      TITLE: 'Réseau résilient, autogéré et anonyme luttant contre la répression et la censure',
      KNOWLEDGE_CARD: {
        TITLE: 'Connaissances',
        DESCRIPTION: 'Coldwire a pour but de fournir et de centraliser des connaissances de base importantes sur l\'autodéfense en ligne et sur le terrain, pour les activistes, les journalistes et les minorités à risque.',
        BUTTON: 'Lire notre brochure'
      },
      SERVICES_CARD: {
        TITLE: 'Services',
        DESCRIPTION: 'Parallèlement à notre travail de diffusion des connaissances, nous développons et fournissons des services décentralisés, chiffré et anonymes pour tous·tes.',
      }
    },
    DONATE: {
      TITLE: 'Aidez-nous à maintenir notre infrastructure et à financer d\'autres collectifs autonomes !',

      CRYPTO: 'Crypto-monnaies',
      CARD: 'Carte de crédit',
      BANK: 'Virment',
      BANK_INFO: '(Important pour notre hôte fiscale !)',

      TEXT_1: 'Coldwire est très coûteux à entretenir et nous avons toujours besoin d\'argent pour archiver notre autonomie complète.',
      TEXT_2: 'Actuellement nous avons un serveur dédié, il nous coûte 64€/mois, nous avons 3 noms de domaine (coldwire.org, coldnet.org, cdw.re) qui nous coûtent 15€/an chacun et ensuite nous avons notre serveur proxy aux Pays-Bas que nous payons 5€/mois et un autre pour le même prix pour gérer le réseau. Au total, nous payons 77,75€ par mois.',
      TEXT_3: 'Le problème est que nous ne possédons aucun de ces serveurs, ce qui peut être dangereux à long terme, même si tout ce qui est sensible est chiffré, les IP sont enregistrées par les hébergeurs, etc. Notre objectif est d\'atteindre notre autonomie complète en ayant notre propre salle de serveurs avec notre propre matériel et pourquoi pas notre propre AS (Autonomous Number) avec nos propres IPs. Mais tout cela coûte cher, louer un local, acheter des serveurs de seconde main, des switchs et des disques dur neufs, payer une connexion internet illimité, l\'électricité, c\'est super cher. Bien sûr, tous les outils que nous développons peuvent être hébergés par vous-même et nous vous encourageons à héberger vos propres instances ! Mais certaines personnes et groupes ne peuvent pas se permettre de louer un serveur ou n\'ont pas les compétences nécessaires, c\'est pourquoi nous avons besoin d\'une infrastructure fiable à laquelle nous pouvons faire confiance.',
      TEXT_4: 'En dehors de cela, nous aimerions vraiment pouvoir faire des dons ou fournir des services (machines virtuelles, serveurs web) à d\'autres collectifs, projets et associations qui ont les mêmes valeurs que nous.',
      TEXT_5: 'En faisant un don, vous nous permettez de poursuivre activement notre mission de développer des moyens résilients, sûrs, gratuits et accessibles, pour aider des personnes et collectifs du monde entier à s\'organiser en toute sécurité.'
    },
    ABOUT: {
      TITLE: 'Qui sommes-nous, que faisons-nous et pourquoi cette question est-elle légitime ?'
    },
    CONTRIBUTE: {
      TITLE: 'Intéressé·e par la sécurité, anticapitaliste, pas transphobe, dev ? Rejoignez-nous !'
    }
  },

  LINKS: {
    RESSOURCES: 'Ressources',
    CONTRIBUTE: 'Contribuez',
    ABOUT: 'À propos',
    DONATE: 'Faire un don',
  },

  MENUS: {
    RESSOURCES: {
      KNOWLEDGE_TITLE: 'Connaissances',
      SERVICES_TITLE: 'Services',
      SOON: 'Bientôt !'
    }
  },

  RESSOURCES: {
    ZINE: {
      TITLE: 'Défense Collective',
      DESCRIPTION: 'Brochure sur les tactiques de défense en ligne et sur le terrain'
    },
    BLOC: 'Stockage de fichiers chiffré, décentralisés et anonymes',
    CINNY: 'Communication sécurisée et indépendante, connectée via Matrix'
  },

  FOOTER: {
    FOLLOW_US: 'Suivez nous sur Mastodon',
    TERMS: 'Terms d\'utilisation',
    LICENSE: 'Tout matériel est sous licence CC BY-NC-SA 2.0 (2022)'
  },
}