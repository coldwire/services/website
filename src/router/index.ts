import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/contribute',
      component: () => import('../views/ContributeView.vue')
    },
    {
      path: '/about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/donate',
      component: () => import('../views/DonateView.vue')
    },
    {
      path: '/ressources',
      children: [
        {
          path: 'bloc',
          name: 'bloc',
          component: () => import('../views/ressources/BlocView.vue')
        },
        {
          path: 'Cinny',
          component: () => import('../views/ressources/CinnyView.vue')
        },
        {
          path: 'zine',
          component: () => import('../views/ressources/ZineView.vue')
        }
      ]
    },
  ]
})

export default router
