import { defineStore } from 'pinia'

export const useMenuStore = defineStore('menu', {
  state: () => ({
    id: '',
    pos: {
      top: 'unset',
      bottom: 'unset',
      left: 'unset',
      right: 'unset'
    }
  }),
  getters: {
    getId: (state) => state.id,
  },
  actions: {
    hide() {
      if (this.id.length > 0) {
        this.id = ''
        this.pos = {
          top: 'unset',
          bottom: 'unset',
          left: 'unset',
          right: 'unset'
        }
      }
    },
    toggle(id: string, pos: {
      top?: number,
      bottom?: number,
      left?: number,
      right?: number
    }) {
      setTimeout(() => {
        if (this.id.length <= 0 || this.id == undefined) {
          this.id = id
  
          this.pos = {
            top: pos.top ? `${pos.top}px` : 'unset',
            bottom: pos.bottom ? `${pos.bottom}px` : 'unset',
            left: pos.left ? `${pos.left}px` : 'unset',
            right: pos.right ? `${pos.right}px` : 'unset'
          }
        } else {
          this.id = ''
          this.pos = {
            top: 'unset',
            bottom: 'unset',
            left: 'unset',
            right: 'unset'
          }
        }
      }, 100)
    },
  }
})
