FROM node:18-bullseye
WORKDIR /build
COPY . .
RUN npm i
RUN npm run build
CMD ["npm", "run", "dev"]